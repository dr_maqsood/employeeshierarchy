using EmployeesHierachry.Model;
using EmployeesHierachry.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;
using System;
using System.Linq;
using Xunit;

namespace EmployeesHierachry.Test
{
    public class EmployeeTest
    {
        private EmployeeContext _employeeContext;
        private IEmployeeService _employeeService;
        public EmployeeTest()
        {
            InitContext();
        }
        /// <summary>
        /// Iniatilize test context
        /// </summary>
        public void InitContext()
        {
            var builder = new DbContextOptionsBuilder<EmployeeContext>()
                .UseInMemoryDatabase($"EmpTestDB {Guid.NewGuid().ToString()}");
         
            var context = new EmployeeContext(builder.Options);
            var employees = new Employee[]
            {
                new Employee { Id = 100, Name = "Alan", ManagerId = 150},
                new Employee { Id = 220, Name = "Martin", ManagerId = 100},
                new Employee { Id = 150, Name = "Jamie", ManagerId = null},
                new Employee { Id = 275, Name = "Alex", ManagerId = 100},
                new Employee { Id = 400, Name = "Steve", ManagerId = 150},
                new Employee { Id = 190, Name = "David", ManagerId = 400},
            };
            foreach (var employee in employees)
            {
                context.Add(employee);
            }
            int changed = context.SaveChanges();
            _employeeContext = context;

            _employeeService = new EmployeeService(_employeeContext);
        }

        [Fact]
        public async void Emplyees_Has_CEO()
        {
            var employees = await _employeeService.GetEmployees();
            var ceo = employees.FirstOrDefault(x => x.ManagerId == null);
            Assert.True(ceo != null);
        }

        [Fact]
        public async void Emplyees_Has_Manager()
        {
            var employees = await _employeeService.GetEmployees();
            var employee = employees.FirstOrDefault(x => x.ManagerId == 150);
            Assert.True(employee != null);
        }

        [Fact]
        public async void CEO_Has_NO_Manager()
        {
            var employees = await _employeeService.GetEmployees();
            var ceo = employees.FirstOrDefault(x => x.ManagerId == null);
            Assert.True(ceo.ManagerId == null);
        }
    }
}