﻿using EmployeesHierachry.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmployeesHierachry.Services
{
    public interface IEmployeeService
    {
        /// <summary>
        /// Get all employees
        /// </summary>
        /// <returns></returns>
        Task<List<Employee>> GetEmployees();
    }
}