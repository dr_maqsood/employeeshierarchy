﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EmployeesHierachry.Model;
using Microsoft.EntityFrameworkCore;

namespace EmployeesHierachry.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly EmployeeContext _employeeContext;

        /// <summary>
        /// EmployeeSerivce constructor
        /// </summary>
        /// <param name="employeeContext"></param>
        public EmployeeService(EmployeeContext employeeContext)
        {
            _employeeContext = employeeContext;
        }
        /// <summary>
        /// Get all employees
        /// </summary>
        /// <returns></returns>
        public async Task<List<Employee>> GetEmployees()
        {
            return await _employeeContext.Employees.ToListAsync();
        }
    }
}