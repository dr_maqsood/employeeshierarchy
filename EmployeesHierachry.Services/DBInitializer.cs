﻿using EmployeesHierachry.Model;
using System.Linq;

namespace EmployeesHierachry.Services
{
    public class DBInitializer
    {
        public static void Initialize(EmployeeContext context)
        {
            context.Database.EnsureCreated();

            //Is there any employee in database
            if(context.Employees.Any())
            {
                //Database has been seeded
                return;
            }           
            
            var employees = new Employee[]
            {
                new Employee { Id = 100, Name = "Alan", ManagerId = 150},
                new Employee { Id = 220, Name = "Martin", ManagerId = 100},
                new Employee { Id = 150, Name = "Jamie", ManagerId = null},
                new Employee { Id = 275, Name = "Alex", ManagerId = 100},
                new Employee { Id = 400, Name = "Steve", ManagerId = 150},
                new Employee { Id = 190, Name = "David", ManagerId = 400},
            };
            foreach (var employee in employees)
            {
                context.Add(employee);
            }
            //save employees in database
            context.SaveChanges();
        }
    }
}