﻿using System.Threading.Tasks;
using EmployeesHierachry.Services;
using Microsoft.AspNetCore.Mvc;

namespace EmployeesHierarchy.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEmployeeService _employeeService;

        /// <summary>
        /// EmployeeContext constructor
        /// </summary>
        /// <param name="employeeContext"></param>
        public HomeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }
        /// <summary>
        /// Get all employees view
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            return View(await _employeeService.GetEmployees());
        }        
    }
}