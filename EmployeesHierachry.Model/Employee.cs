﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeesHierachry.Model
{
    public class Employee
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None), Key]
        public int Id { get; set; }
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }
        public int? ManagerId { get; set; }
    }
}